<?php
/**
 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class modBackgroundStretchHelper
{
	static $bsLoaded = false;
	
	/**
	* Load the backtretch plugin if needed
	*/
	static function loadBackstretchLibrary($debug = false)
	{	
		if (self::$bsLoaded) {
			return;
		}
		
		$doc = JFactory::getDocument();
		
		if ($debug) {
			$doc->addScript(JURI::root(true).'/media/syw_backgroundstretch/js/jquery.backstretch.js');
		} else {
			if (version_compare(JVERSION, '3.2.0', 'ge')) {
				$doc->addScriptVersion(JURI::root(true).'/media/syw_backgroundstretch/js/jquery.backstretch.min.js');
			} else {
				$doc->addScript(JURI::root(true).'/media/syw_backgroundstretch/js/jquery.backstretch.min.js');
			}
		}
		
		self::$bsLoaded = true;
	}
	
	/**
	 * Backstretch still initialization
	 */
	static function getStillJavascript($image, $target = 'body')
	{
		$html = 'jQuery(document).ready(function($) {';
	
		$html .= '$("'.$target.'").backstretch(';
		
		$html .= '"'.$image.'"';
	
		$html .= ');';
		
		$html .= '});';
	
		return $html;
	}
	
	/**
	 * Backstretch animation initialization
	 */
	static function getAnimationJavascript($images, $target = 'body', $transition = 'fade', $transitionDuration = 250, $duration = 3000, $high_resolution = false)
	{
		$html = 'jQuery(document).ready(function($) {';
	
		$html .= '$("'.$target.'").backstretch([';
		
		foreach ($images as $width => $image_array) {
			
			if ($width == 'all') {
				foreach ($image_array as $image) {
					$html .= '"'.$image.'",';
				}
			} else {
				foreach ($image_array as $image) {
					if ($high_resolution) {
						$html .= '{ width: '.$width.', url: "'.str_replace('.', '@2x.', $image).'", pixelRatio: 2 },';
						$html .= '{ width: '.$width.', url: "'.$image.'", pixelRatio: 1 },';
					} else {
						$html .= '{ width: '.$width.', url: "'.$image.'" },';
					}
				}
			}
		}
		
		$html = rtrim($html, ',');
		
		$html .= '], { duration: '.$duration.', transition: "'.$transition.'", transitionDuration: '.$transitionDuration.', preload: 1 }';

		$html .= ');';
		
		$html .= '});';
	
		return $html;
	}
}
?>