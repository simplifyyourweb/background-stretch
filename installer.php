<?php
/**
 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Script file of the Background Stretch module
 */
class mod_backgroundstretchInstallerScript
{		
	static $version = '1.2.0';
	static $minimum_needed_library_version = '1.4.5';
	static $download_link = 'http://www.simplifyyourweb.com/index.php/downloads/category/23-libraries';
	
	/**
	 * Called before an install/update method
	 *
	 * @return  boolean  True on success
	 */
	public function preflight($type, $parent)
	{
		// check if syw library is present
		
		if (!JFolder::exists(JPATH_ROOT.'/libraries/syw')) {
			
			$message = JText::_('SYW_MISSING_SYWLIBRARY').'.<br /><a href="'.self::$download_link.'" target="_blank">'.JText::_('SYW_DOWNLOAD_SYWLIBRARY').'</a>.';
			JFactory::getApplication()->enqueueMessage($message, 'error');
				
			return false;
		} else {
			jimport('syw.version');
			if (SYWVersion::isCompatible(self::$minimum_needed_library_version)) {
				
				JFactory::getApplication()->enqueueMessage(JText::_('SYW_COMPATIBLE_SYWLIBRARY'), 'message');
				
				return true;
			} else {			
				
				$message = JText::_('SYW_NONCOMPATIBLE_SYWLIBRARY').'.<br />'.JText::_('SYW_UPDATE_SYWLIBRARY').' '.JText::_('SYW_OR').' <a href="'.self::$download_link.'" target="_blank">'.strtolower(JText::_('SYW_DOWNLOAD_SYWLIBRARY')).'</a>.';
				JFactory::getApplication()->enqueueMessage($message, 'error');
								
				return false;
			}
		}
	}
	
	/**
	 * Called after an install/update method
	 *
	 * @return  boolean  True on success
	 */
	public function postflight($type, $parent) 
	{	
		echo '<p style="margin: 10px 0 20px 0">';
		echo '<img src="../modules/mod_backgroundstretch/images/logo.png" style="float: none" />';
		echo '<br /><br /><span class="label">'.JText::sprintf('MOD_BACKGROUNDSTRETCH_VERSION', self::$version).'</span>';
		echo '<br /><br />Olivier Buisard @ <a href="http://www.simplifyyourweb.com" target="_blank">Simplify Your Web</a>';
		echo '</p>';
		
		if ($type == 'update') {
			
			// upgrade warning
			
			JFactory::getApplication()->enqueueMessage(JText::_('MOD_BACKGROUNDSTRETCH_WARNING_RELEASENOTES'), 'warning');
			
			// delete unnecessary files
			
			$files = array(
				'/modules/mod_backgroundstretch/js/jquery.backstretch.min.js',
				'/modules/mod_backgroundstretch/js/license.txt'
			);
			
			foreach ($files as $file) {
				if (JFile::exists(JPATH_ROOT.$file) && !JFile::delete(JPATH_ROOT.$file)) {
					JFactory::getApplication()->enqueueMessage(JText::sprintf('MOD_BACKGROUNDSTRETCH_ERROR_DELETINGFILEFOLDER', $file), 'warning');
				}
			}
		}
		
		return true;
	}	
	
	/**
	 * Called on installation
	 *
	 * @return  boolean  True on success
	 */
	public function install($parent) {}
	
	/**
	 * Called on update
	 *
	 * @return  boolean  True on success
	 */
	public function update($parent) {}
	
	/**
	 * Called on uninstallation
	 *
	 * @param   JAdapterInstance  $adapter  The object responsible for running this script
	 */
	public function uninstall($parent) {}
}
?>