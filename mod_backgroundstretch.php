<?php
/**
 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('syw.libraries');
jimport('syw.utilities');

$isMobile = SYWUtilities::isMobile();

$show_on_mobile = $params->get('show_on_mobile', 1);
if (($isMobile && $show_on_mobile == 0) || (!$isMobile && $show_on_mobile == 2)) {
	return;
}

require_once (dirname(__FILE__).'/helper.php');

$urlPath = JURI::base()."modules/mod_backgroundstretch/";

$doc = JFactory::getDocument();

// jQuery library loading

JHtml::_('jquery.framework');

// Backstretch plugin loading

$config = JFactory::getConfig();
$debug  = (boolean) $config->get('debug');

modBackgroundStretchHelper::loadBackstretchLibrary($debug);

// declaration loading

$target = $params->get('applyto', 'body');

$image_folder = $params->get('imagefolder', 'images');
$image_file = $params->get('imagefile', '');

if (!empty($image_file)) {	
	$doc->addScriptDeclaration(modBackgroundStretchHelper::getStillJavascript(JURI::base().$image_file, $target));
} else {
	jimport( 'joomla.filesystem.folder' );
	
	$directory = JPATH_SITE.'/'.$image_folder;
	
	$image_list = array();
	
	if (JFolder::exists($directory)) {
		$images = JFolder::files($directory);
		
		if ($params->get('randomize', 1)) { // randomize list of images
			shuffle($images);
		}
		
		$image_list['all'] = array();
		
		foreach($images as $image) {
			$extension = JFile::getExt($image);
			if ($extension == 'jpg' || $extension == 'png') {
				$image_list['all'][] = JURI::base().$image_folder.'/'.$image;
			}
		}
		
		$transitions = $params->get('transitions', 'fade');
		if (is_array($transitions)) {
			$transitions = implode("|", $transitions);
		}
		
		$transitionDuration = $params->get('fade', 750);
		$duration = $params->get('interval', 3000);
		
		$doc->addScriptDeclaration(modBackgroundStretchHelper::getAnimationJavascript($image_list, $target, $transitions, $transitionDuration, $duration));
	} else {
		JFactory::getApplication()->enqueueMessage(JText::sprintf('MOD_BACKGROUNDSTRETCH_FOLDERDOESNOTEXIST', JURI::base().$image_folder));
	}
}

// force z-index to the element the jQuery plugin is applied to

$zindex = $params->get('zindex', '');
if (!empty($zindex)) {
	$doc->addStyleDeclaration($target.' { z-index: '.$zindex.' !important; }');
}

require(JModuleHelper::getLayoutPath('mod_backgroundstretch', $params->get('layout', 'default')));
?>
